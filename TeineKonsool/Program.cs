﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineKonsool
{
    class Program
    {
        static void Main(string[] args)
        {
            // Kommentaar
            /* Ploki 
             kommentaar
             
             */
            Console.WriteLine("Tere Marilii!");
            Console.WriteLine(Liida(7, 4));

            // Liitkommentaar: (kui esimene kaldkriips ära võtta, muutub esimene lause kommentaariks ja teine aktiivseks)
            //*
            Console.WriteLine("Täna on ilus ilm");
            //*/ Console.WriteLine("Täna on kole ilm");
            // esimene ja põhiline mõiste - muutuja
            // muutuja on nimeline mälupiirkond

            int arv = 7; // muutuja defineerimine ja väärtustamine
            int teinearv; // muutuja defineerimine (ilma väärtustamata) - halb komme
            teinearv = arv * 10; // muutuja väärtustamine (avaldisega)
            Console.WriteLine(teinearv); // muutuja kasutamine (väljanäitamiseks)

            // teine fundamentaalmõiste - andmetüüp
            int // andmetüüp - mis asi see on
            kolmasArv // muutja nimi
            = 100 // algväärtus (avaldis)
            ; // lauset lõpetav semikoolon

            /* andmetüüp - defineerib nelja asjandust
             * 1. Kui palju bitte (baite) vaja on - maht
             * 2. Mida need bitid (baidid) tähendavad - semantika
             * 3. Väärtuste hulk - süntaks, domain
             * 4. Mida nendega teha saab - operatsioonid, tehted jms
             */
            Console.WriteLine(kolmasArv.GetType().FullName);

            decimal d1 = 10;
            Decimal d2 = d1;

            DateTime täna = DateTime.Today;
            Console.WriteLine(täna);

            // F12 = go to definition, F1 = avab docs veebilehitsejas

            char a = 'A'; // char on arv; 65 000 võimalikku väärtust
            Console.WriteLine(++a);
            Console.WriteLine('a' - 'A');

            // järgmine kole ja fundamentaalne asi on avaldis

            /* avaldis on lausend, mis on pandud kokku
             * literaalidest - arvud ja muud "konstandid"
             * muutujatest
             * tehetest
             * sulgudest
             * funktsioonidest
             */

            Console.WriteLine((arv = teinearv) * 7);

            Console.WriteLine(10 % 3); // jagamise jääk
            Console.WriteLine(10 / 3); // täisarve jagades on vastus ALATI täisarv

            Console.WriteLine(10 % 7);
            Console.WriteLine(10 / 7);

            /* Avaldis on keeruline loom
             Tehtemärke on jube palju
             Binaarsed tehted - kaks operandi
              ? - * / % += -= *=    Tehetel on kaks poolt: mida ta teeb ja mida ta veel teeb (arvutab -> tulemus + kõrvaleffekt (mõni muutuja muudab oma väärtust))
             Unaarsed tehted - üks operand
              ! - ++ --     (! muudab vastupidiseks)
             Ternaarne tehe - kolm operandi
              ?: (Elvise tehe)
              boolean-avaldis (jah/ei) ? jah-väärtus : ei-väärtus !!NB!! Tegemist on avaldisega ja avaldist saab kasutada teiste avaldiste sees
              = omistamistehe
              == võrdlemistehe
              === samasustehe (JavaScriptis, C# keeles pole)
            */

           /* 
            Console.WriteLine("Palju Sa palka saad?");
            decimal palk = decimal.Parse(Console.ReadLine());
            string tulemus = palk < 1000 ? "vaene" : "rikas";
            string kuhu =
                palk > 1000 ? "õhtust sööma" :
                palk > 100 ? "kinno" :
                palk > 10 ? "burksi" :
                "mitte kuhugi";
            Console.WriteLine("Sinuga läheme " + kuhu);
            */

            kolmasArv += (arv += 7) * 2;
            // = on omistamisTEHE
            Console.WriteLine((arv = kolmasArv) + (teinearv * arv));

            arv = 20;
            Console.WriteLine(arv++); // 20. Kasutatakse arvu eelmist väärtust
            Console.WriteLine(++arv); // 22. Kasutatakse ühevõrra suurendatud väärtust
            // Järgmise võtmine

            // Tehete prioriteedid
            // 3 + 4 * 2        Korrutamine on prioriteetsem (esmasem) ja tehakse enne

            //  */&
            //  +-
            //  & - loogiline korrutamine
            //  | - loogiline liitmine

            // Aritmeetikatehted on vasakassotsiatiivsed e tehakse samal tasemel vasakult paremale

            // (avaldis1) + (avaldis2) +7; Tehted tehakse vasakult paremale (vasakassotsiatiivsed)

            // 12 / 6 / 3 (vasakult paremale)

            // Omistamistehe on paremassotsiatiivne

            // a = b = c        paremassotsiatiivne  b = c ja siis a = b

            // KUI ON VAJA KINDEL OLLA TEHETE JÄRJEKORRAS, siis kasutatakse SULGUSID


        }

        /// <summary>
        /// See on liitmise funktsioon
        /// </summary>
        /// <param name="yks">Esimene liidetav</param>
        /// <param name="teine">Teine liidetav</param>
        /// <returns>Summa</returns>
        static int Liida(int yks, int teine)
        {
            return yks + teine;
        }

    }
}
