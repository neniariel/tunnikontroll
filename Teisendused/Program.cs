﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Teisendused
{
    class Program
    {
        static void Main(string[] args)
        {
            // aeg-ajalt on vaja andmetüüpe ühest teiseks teisendada
            // või ka tüübi enda sees muuta

            #region Kommenteerisin välja - var
            //         kõigepealt - mis asi on var

            //var ilusMuutuja = 7; // süsteem otsustab ise, mis tüüpi ta on. Hetkel on int, 7M oleks Decimal jne. Muutuja ilusMuutuja on mul selleks, et teda uurida
            //Console.WriteLine(ilusMuutuja.GetType().Name);
            //Console.WriteLine("ilusMuutuja väärtus on {0}", ilusMuutuja);
            //var teine = ilusMuutuja; // Kindlasti sama tüüpi, mis ilusMuutuja

            //var henn = new { Nimi = "Henn", Vanus = 64 }; // Saan defineerida muutujaid, öelda talle tüübi nime
            //Console.WriteLine(henn.GetType().Name);

            //foreach (var x in henn.GetType().GetProperties())
            //    Console.WriteLine(x.Name);
            #endregion

            //Kasulik käsk - rename. Oskab ise muutujaid intelligentselt leida

            // arvusid saab teisteks arvudeks teisendada lihtsalt
            int i = 1000;
            long l = i; // toimub ilmutamata tüübiteisendus

            i = (int)(l + l*2); // ilmutatud tüübiteisendus e casting operation
            // casting on suhteliselt kõrge prioriteediga, seepärast avaldis sulgudesse

            l = long.MaxValue; // suurim long arv
            i = (int)l;
            Console.WriteLine(i);

            i = int.MaxValue;
            i++;
            Console.WriteLine(i);

            double d = 10.0;
            i = (int)d;

            // Samaliigilisi arve saab teisendada ilmutamata väiksemast suuremasse
            // suuremast väiksemasse või liikide vahel tuleb castida (ilmutatult teisendada)

            d = i; // integeri saab omistada kõigile
            decimal dets = i;
            d = (double)dets; // võib ka d = (float)dets; olla

            #region Arvuti ja arvud
            // Kuidas arvuti arvudest aru saab:
            //0000 0011 - kolm (1+2)
            //0001 0100 - kakskümmend (16+4)
            //  saja kahekümnekaheksased kuuekümneneljased kolmekümnekahesed kuueteistkümnesed   kaheksased neljased kahesed ühesed

            // -5
            //0000 0101 +5
            //1111 1011 -5 (Kõik vastupidised, plus 1 - tulemus on negatiivne arv) 1+1 = 10 Negatiiveid arve (-1 kuni -128) on ühe võrra rohkem kui positiivseid arve (1 kuni 127)

            // Kõige suurem positiivne arv: 0111 1111 1111 1111 - suur arv
            //                                        1111 1111 - miinus 1 
            #endregion

            string tekst = "100";
            // i = (int)tekst; // see ei toimi - stringi ei saa castida teistesse tüüpidesse
            // tekst = d; // see ka ei toimi

            tekst = d.ToString(); // Kõiki asju saab teisendada stringiks
            Console.WriteLine(tekst);

            i = int.Parse("14"); // see teisendab teksti int asjaks
            Console.WriteLine(i * 2);
            DateTime dt = DateTime.Parse("March 7 1955", new CultureInfo("en-us"));
            Console.WriteLine(dt);

            d = double.Parse("14.0", new CultureInfo("en-us"));

            Console.WriteLine("Millal oled Sa sündinud?");
            string sünniaeg = Console.ReadLine(); //
            //DateTime sünnipäev = DateTime.Parse(sünniaeg);
            //Console.WriteLine($"Sa siis oled sündinud sellisel päeval {sünnipäev.DayOfWeek}");

            // tryparse võimaldab proovida ja otsustada
            //if(DateTime.TryParse(sünniaeg, out DateTime sündinud)) // Kui õnnestub, siis kirjutab muutuja
            //{
            //    Console.WriteLine("Sinu sünnipäev on siis " + sündinud.ToShortDateString());
            //}
            //else Console.WriteLine("See pole miski sünniaeg");

            // sama asi Elvisega

            Console.WriteLine(
                DateTime.TryParse(sünniaeg, out DateTime x) ? $"Sa oled siis sündinud {x}" :
                "See pole miski kuupäev"
                );

            // kolmas viis teisendada on kasutada staatilist klassi Convert kus on SUUR HULK teisendamisfunktsioone
            int teine = Convert.ToInt32("100");
            decimal teineD = Convert.ToDecimal("100,5");
            // jne jne

            // convert klassis olevad funktsioonid võimaldavad teisendada millest iganes, millesse iganes
            // kui see on võimalik

            // ÄRA KASUTA Convert klassi teisendusfunktsioone, kui suudad, sest
            // 1. need on tehtud "kehvemate" keelte harjutamise tarbeks (natu VB-s on ToInt sihukesed)
            // 2. neil ei ole seda TryParse moodi asjandust
            // 3. casting ja parse on "standard"-viisid asju teisendada ja seepärast pigem kasutada neid
        }
    }
}
