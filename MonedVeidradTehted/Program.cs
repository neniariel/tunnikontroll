﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonedVeidradTehted
{
    class Program
    {
        static void Main(string[] args)
        {
            // paar tehet, mis on harjumatud ja millega tuleb harjuda

            // loogikatehe ja && ja või || - kirjutatakse kahekordselt

            // mida teevad tehted & ja |

            Console.WriteLine(5 & 6); // trükib 4
            Console.WriteLine(5 | 6); // trükib 7
            Console.WriteLine(5 ^ 6); // trükib 3

            // kuidas arvuti sisemiselt arvudest aru saab
            // 6 - 0110     6 & 5 - 0100 - 4 (null võidab)      5 ^ 6 - 0011 - 3
            // 5 - 0101     5 | 6 - 0111 - 7 (üks võidab)
            // bitwise logical operations
            // ^ bitwise xor (annab vastandi)

            string nimi = "Marilii Saar";
            //Console.WriteLine(nimi[1]+0);

            string failinimi = @"C:\hobune\nimekiri.txt"; //@ ees tähendab, et ei pea kaldkriipse / erimärke escape'ima
            string jutt1 = "Luts jutustab: \"Kui Arno jne jne\""; // jutumärk escape'itud stringis
            string jutt2 = @"Luts jutustab: ""Kui Arno jne jne"""; // jutumärk escape'imata stringis topelt

            Console.WriteLine("Henn on ilus poiss \nja tark kahh"); // line feed ehk LF ehk char(10)
            Console.WriteLine("Henn on ilus poiss\rAnts"); // carriage return ehk CR ehk char(13)

            // string on kahtlane loom - tema on MUUTUMATU
            string nimi2 = "Marilii";
            nimi2 += " ";
            nimi2 += "Saar";
            // Küllaltki mälu raiskav liigutus

            Console.WriteLine(nimi);
            Console.WriteLine(nimi2);

            Console.WriteLine(nimi == nimi2 ? "samad" : "erinevad"); // Javas erinevad
            Console.WriteLine(nimi.Equals(nimi2, StringComparison.InvariantCultureIgnoreCase) ? "samad" : "erinevad"); // Ka Javas samad; StringComparison.InvariantCultureIgnoreCase - ignoreerib suurt/väikest tähte

            Console.Write("Tere "); //Ei lisa reavahetust
            Console.WriteLine("Marilii"); // Lisab lõppu reavahetuse

            int arv1 = 7;
            decimal arv2 = 10.777M;

            // Placeholderid stringis (stringFormat)
            Console.WriteLine("Arvud on sellised {0} ja {1:F2}", arv1, arv2); // F2 palub anda 2 kohta pärast koma

            string tulemus;
            tulemus = string.Format("Arvud on sellised {0} ja {1:F2}", arv1, arv2);
            Console.WriteLine(tulemus);

            // Placeholderid-avaldised - interpoleeritud string
            Console.WriteLine($"Arvud on sellised {arv1} ja {arv2:F2}");
            tulemus = $"Arvud on sellised {arv1} ja {arv2:F2}";
            Console.WriteLine(tulemus);

        }
    }
}
